import os
from logger import *
from process import *

class Debugger:

	current_process = None
	is_running 		= False
	exePath 		= None

	software_breakpoints = {}
	memory_breakpoints = {}
	hardware_breakpoints = [
		[-1, -1, -1],
		[-1, -1, -1],
		[-1, -1, -1],
		[-1, -1, -1]
	]
	current_scope = {}

	debug_mode = {
		"create_suspended" : True
	}
	bpt_state = (-1, -1) # type, tid

	def get_error(self):
		return windll.kernel32.GetLastError()
	def run_exe(self, exePath, argv):
		#waiter_thread = threading.Thread(target = thread_debugwait, args=(exePath, argv))
		#waiter_thread.start()
		debug_print("Program running : %s" % exePath)
		self.scope_register('exePath', exePath)
		self.current_process = dProcess()
		self.current_process.exePath = exePath
		self.exePath = exePath
		self.current_process.argv = argv
		self.current_process.bind(CREATE_PROCESS_DEBUG_EVENT, self.cb_running)
		self.current_process.bind(EXIT_PROCESS_DEBUG_EVENT  , self.cb_killed )
		result = self.current_process.run()
		return self.current_process

	def cb_running(self, process, debug_event, result):
		self.is_running = True
		for sbpt in self.software_breakpoints:
			ret = self.current_process.software_breakpoint(sbpt)
			if ret == False:
				debug_print("sbp failed!")
				continue
			print "Breakpoint assigned at %x" % sbpt
		for mbpt in self.memory_breakpoints:
			self.current_process.memory_breakpoint(mbpt, self.memory_breakpoints[mbpt])
		self.current_process.apply_hardware_breakpoint(self.hardware_breakpoints)
		debug_print("Process started : %s" % process.exePath)

	def cb_killed(self, process, debug_event, result):
		self.is_running = False
		debug_print("Process ended : %s" % process.exePath)	

	def process_formula(self, formula):	
		debug_print("Formula: %s" % formula)
		return eval(formula)
		try:
			return eval(formula, self.current_scope)
		except:
			debug_print("Formula Error: %s" % formula)
			return None
	def set_exe_path(self, newExePath):
		if os.path.isfile(newExePath): # check file existance
			self.exePath = newExePath
			debug_print("Executable %s : loaded" % newExePath)
		else:
			debug_print("Executable %s : file not found" % newExePath)

	def scope_register(self, key, value):
		self.current_scope[key] = value

	def software_breakpoint(self, addr):
		self.software_breakpoints[addr] = 1

	def memory_breakpoint(self, addr):
		self.memory_breakpoints[addr] = 1

	def hardware_breakpoint(self, addr, size, subtype=EXECUTE):
		if size not in [1, 2, 4]:
			debug_print("Invalid hardware breakpoint size (allow size : 1, 2, 4)")
			return False
		bpt_index = 0

		# [0] : addr
		# [1] : type
		# [2] : size
		for breakpoint in self.hardware_breakpoints:
			if breakpoint[0] == -1:
				breakpoint[0] = addr
				if subtype == EXECUTE:
					breakpoint[1] = 0b00
				elif subtype == READ:
					breakpoint[1] = 0b01
				elif subtype == READWRITE:
					breakpoint[1] = 0b11
				breakpoint[2] = size
				break

		if self.is_running == True:
			if self.current_process.apply_hardware_breakpoint(self.hardware_breakpoints) == False:
				return False
			else:
				return True

	def query_mode(self, mode):
		global debug_mode
		if mode in debug_mode:
			debug_print("mode %s : %s" % (mode, debug_mode[mode]))
		else:
			debug_print("mode %s not found" % mode)

	def set_mode(self, key, value):
		global debug_mode
		debug_mode[key] = value
	def mem_region(self):
		if self.is_running == True:
			return self.current_process.mem_region()
	def dump_area(self, start, size):
		data = self.current_process.dump_area(start, size)

	def kill(self):
		if self.current_process.kill():
			self.is_running = False

	def continue_process(self):
		self.current_process.continue_process()

	def get_regs(self):
		return self.current_process.get_regs()


