from ctypes import *
from structure import *

STD_OUTPUT_HANDLE = -11
csbi = CONSOLE_SCREEN_BUFFER_INFO()
def terminal_size():
	windll.kernel32.GetConsoleScreenBufferInfo(
		windll.kernel32.GetStdHandle(STD_OUTPUT_HANDLE),
		byref(csbi))
	cols = csbi.Window.Right - csbi.Window.Left + 1;
	rows = csbi.Window.Bottom - csbi.Window.Top + 1;
	return cols,rows
