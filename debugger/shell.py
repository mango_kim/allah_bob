#!/usr/bin/python
# import "module" when common module
# from "module" import "something" when local module
# ctypes included
import sys, os.path, os
import argparse
import threading
from ctypes import *
from structure import *
from interactive import shell, choice, banner, autocmd
from logger import *
from disas import *
from debugger import *

banner()
dbg = Debugger()
scriptFile = None

parser = argparse.ArgumentParser(prog='run.py')
parser.add_argument('exe', nargs='?', help='exe file name')
parser.add_argument('--script', nargs='?', help='initial script file to execute')
waiter_thread = None
args = vars(parser.parse_args())
if 'exe' in args and args['exe'] is not None:
	dbg.set_exe_path(args['exe'])
else:
	exePath = None
if 'script' in args and args['script'] is not None:
	scriptFile = open(args['script'], 'r')

while True:
	if scriptFile is not None:
		data = scriptFile.readline().strip()
		result = [data.split(' ',1)[0] + autocmd(data.split(' ', 1)[0]), data.split(' ')[1:]]
		if data == '':
			scriptFile = None
			continue
	else:
		result = shell()
	cmd, argv = result
	print result
	if cmd == 'quit':
		if dbg.is_running == False:
			exit()
		print "Running"
		if choice("Process is running. Do you really want to quit?") == True:
			dbg.kill() # kill process
	elif cmd == 'file':
		dbg.set_exe_path(' '.join(argv))
	elif cmd == 'run':
		if dbg.is_running == True:
			if choice("Process is running. kill and restart process?") == True:
				dbg.kill()
			else:
				continue
		if dbg.exePath is None:
			debug_print("Choose exe file first:")
			debug_print("ex) file test\\notepad.exe")
			continue
		dbg.run_exe(dbg.exePath, argv)
	elif cmd == 'mode':
		if len(argv) == 1:
			dbg.query_mode(argv[0])
		else:
			dbg.set_mode(argv[0], ' '.join(argv[1:]))
	elif cmd == 'hbreakpoint':
		if len(argv) < 1:
			debug_print("Invalid usage. \nusage: hbreakpoint addr size [execute]")
			continue
		if len(argv) < 2:
			bpt_size = 1
		else:
			bpt_size = eval(argv[1])
		if len(argv) < 3:
			bpt_type = EXECUTE
		else:
			argv[2] = argv[2].lower()
			if argv[2] == 'r':
				bpt_type = READ
			elif argv[2] == 'rw':
				bpt_type = READWRITE
			elif argv[2] == 'x':
				bpt_type = EXECUTE
		bpt_addr = eval(argv[0])
		dbg.hardware_breakpoint(bpt_addr, bpt_size, bpt_type)
	elif cmd == 'breakpoint':
		map(dbg.software_breakpoint, map(eval, argv))
	elif cmd == 'mbreakpoint':
		map(dbg.memory_breakpoint, map(eval, argv))
	elif cmd == 'disassemble':
		# readbin = opener(argv[0],0x100000)
		# dbg.findmain(readbin)
		# dbg.disas(readbin)
		pass
	elif cmd == 'detach':
		current_process.detach()
		dbg.is_running = False
	elif cmd == 'meminfo':
		region = dbg.mem_region()
		for page in region:
			print page
	elif cmd == 'dumpall':
		if len(argv) < 1:
			debug_print("invalid usage.\nusage: dumpall output_file_path")
			continue
		else:
			region = dbg.mem_region()
			try:
				f = open(argv[0], 'wb')
			except:
				debug_print("File open failed : %s" % argv[0])
				continue
			for page in region:
				f.write(struct.pack("<QQ", page[0], page[1]))
				f.write(dbg.dump_area(page[0], page[1]))
	elif cmd == 'dump':
		if len(argv) < 2:
			continue
		area = dbg.dump_area(eval(argv[0]), eval(argv[1]))
	elif cmd == 'continue':
		dbg.continue_process()
	elif cmd == 'info':
		if len(argv) == 0:
			print """info - prints information
	info regs : dumps current register information
	"""
		else:
			if argv[1] == 'regs':
				regs = dbg.get_regs()
	elif cmd == 0:
		pass
	elif cmd == -1:
		debug_print('Invalid command')
	elif cmd == -2:
		debug_print('Ambigous command : %s' % ' '.join(argv[1]))
	elif cmd == -3:
		debug_print('Type \'q\' to exit this inteface')
