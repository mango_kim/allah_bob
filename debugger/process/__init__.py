from ctypes import *
from structure import *
from logger import *
from constant import *
from bpt import *
from dump import *
import threading

class dProcess(dProcess_BPT, dProcess_DUMP):
	pid = 0
	callbacks = {}
	software_breakpoints = {}
	memory_breakpoints = {}
	hardware_breakpoints = [[-1, -1, -1]] * 4 # array
	def __init__(self, handle=None):
		self.handle = handle
		pid = windll.kernel32.GetProcessId(handle)
		self.pid = pid
		self.exePath = None
		self.argv = []
		self.threads = []
		self.is_killed = False

		for i in range(10):
			self.callbacks[i] = []
		self.callbacks [ CREATE_PROCESS_DEBUG_EVENT ].append(self._process_started)
		self.callbacks [ EXIT_PROCESS_DEBUG_EVENT   ].append(self._process_killed)
		self.callbacks [ EXCEPTION_DEBUG_EVENT      ].append(self._pre_exception)
		self.callbacks [ LOAD_DLL_DEBUG_EVENT       ].append(self._loaded_dll)
		self.callbacks [ CREATE_THREAD_DEBUG_EVENT  ].append(self._thread_created)
		self.callbacks [ EXIT_THREAD_DEBUG_EVENT    ].append(self._thread_exit)
		self.callbacks [ UNLOAD_DLL_DEBUG_EVENT     ].append(self._unload_dll)
	def run(self):
		if self.exePath is None:
			return False
		self.debug_loop_thread = threading.Thread(target = self.debug_loop)
		self.debug_loop_thread.start()
	def _thread_created(self, process, debug_event, result):
		debug_print('Thread Created (handle=%d)' % (debug_event.u.CreateThread.hThread))
		thread = dThread(handle=debug_event.u.CreateThread.hThread, base_addr=debug_event.u.CreateThread.lpStartAddress)
		'''for i in range(4):
			self.hardware_breakpoints[0] = [0x411078+i, 0b00, 1]'''
		self.apply_hardware_breakpoint(self.hardware_breakpoints)
		self.threads.append(thread)
	def _thread_exit(self, process, debug_event, result):
		for thread in self.threads:
			if thread.if_exist() == False:
				debug_print('Exit Thread (handle=%d)' % thread.handle)
				self.threads.remove(thread)
				del thread
	def _loaded_dll(self, process, debug_event, result):
		ImageName = "DLLNAMEHERE!"
		debug_print("Dll loaded : %s (handle = %d, base_addr = 0x%08x)" % (ImageName, debug_event.u.LoadDll.hFile, debug_event.u.LoadDll.lpBaseOfDll))
	def debug_loop(self):
		si = STARTUPINFO()
		pi = PROCESS_INFORMATION()
		result = windll.kernel32.CreateProcessA(self.exePath,
			self.exePath + ' ' + ' '.join(self.argv),
			None,
			None,
			True,
			DEFAULT_FLAG | CREATE_SUSPENDED ,
			None,
			None,
			byref(si),
			byref(pi))
		self.handle = None
		if result == 0:
			return False
		self.handle = pi.hProcess
		thread = dThread(pi.hThread, pid=pi.dwThreadId)
		self.pid = pi.dwProcessId
		self.tid = pi.dwThreadId
		self.is_killed = False
		self.threads.append(thread)
		#self.hardware_breakpoints[0] = [0x411078, 0b11, 1]
		self.apply_hardware_breakpoint(self.hardware_breakpoints)
		handle = c_int()
		if windll.kernel32.DuplicateHandle(
			windll.kernel32.GetCurrentProcess(),
			self.handle,
			windll.kernel32.GetCurrentProcess(),
			byref(handle),
			0,
			True,
			2) == False:
			return False
		self.handle = handle

		for addr in self.software_breakpoints:
			if addr is None:
				debug_print("no!")
			self.software_breakpoint(addr)

		windll.kernel32.ResumeThread(pi.hThread)

		# debug event loop!
		while True:
			result = self.get_debug_event()
			if result == -1:
				return
	def dump(self, start, end):
		return
	def _unload_dll(self, process, debug_event, result):
		debug_print("Unload Dll: 0x%08x" % debug_event.u.UnloadDll.lpBaseOfDll)
		return
	def _process_started(self, process, debug_event, result):
		return

	def _pre_exception(self, process, debug_event, result):
		ex = debug_event.u.Exception.ExceptionRecord
		oldprotect = DWORD()

		if ex.ExceptionCode == 0x80000003L: # EXCEPTION_BREAKPOINT
			return self.ex_breakpoint(process, debug_event, result)
		elif ex.ExceptionCode == 0x80000004L: # EXCEPTION_SINGLE_STEP
			debug_print("SINGLE STEP!")
			return self.ex_singlestep(process, debug_event, result)
		elif ex.ExceptionCode == 0x80000001L: #EXCEPTION_GUARD_PAGE
				debug_print("fuck you james gothling")

	def ex_breakpoint(self, process, debug_event, result):
		ex = debug_event.u.Exception.ExceptionRecord
		context = CONTEXT()
		context.ContextFlags = 0x00010007 | 0x00010010
		oldprotect = DWORD()

		print self.software_breakpoints
		if ex.ExceptionAddress in self.software_breakpoints:
			hThread = windll.kernel32.OpenThread(THREAD_GET_CONTEXT | THREAD_SET_CONTEXT, None, debug_event.dwThreadId)
			if windll.kernel32.GetThreadContext(hThread, byref(context)) == 0:
				return
			windll.kernel32.VirtualProtectEx(self.handle, ex.ExceptionAddress, 1, 0x40, byref(oldprotect)) # 0x40 : PAGE_EXECUTE_READWRITE
			size = SIZE_T()
			windll.kernel32.WriteProcessMemory(self.handle, ex.ExceptionAddress, self.software_breakpoints[ex.ExceptionAddress], 1, byref(size))
			windll.kernel32.VirtualProtectEx(self.handle, ex.ExceptionAddress, 1, oldprotect, byref(oldprotect)) 
			context.EFlags ^= 0x100	# set the trap flag
			context.Eip -= 1
			
			if windll.kernel32.SetThreadContext(hThread, byref(context)) == 0:
				debug_print("Continue Failed")
			windll.kernel32.FlushInstructionCache(self.handle, ex.ExceptionAddress, SIZE_T(1))
			debug_print('breakpoint here : 0x%x' % ex.ExceptionAddress)
			windll.kernel32.CloseHandle(hThread)
			result.isContinue = BREAKPOINT
			return
		else:
			debug_print('int3 occured, but not in breakpoint')
			debug_print('addr: 0x%08x' % (ex.ExceptionAddress))
			debug_print(`self.software_breakpoints`)
			return

	def ex_singlestep(self, process, debug_event, result):
		ex = debug_event.u.Exception.ExceptionRecord
		context = CONTEXT()
		context.ContextFlags = 0x00010007 | 0x00010010
		oldprotect = DWORD()

		#while True: debug_print("Single step event")
		hThread = windll.kernel32.OpenThread(THREAD_GET_CONTEXT | THREAD_SET_CONTEXT, None, debug_event.dwThreadId)
		if windll.kernel32.GetThreadContext(hThread, byref(context)) == 0:
			return

		dr6 = context.Dr6
		is_hardware_breakpoint = False
		hardware_breakpoint_index = 0
		for i in map(lambda j: 1<<j, range(4)):
			if dr6 & i:
				is_hardware_breakpoint = True
				#hardware_breakpoint_index = i
				break
			else:
				hardware_breakpoint_index += 1
		context.Dr6 = 0

		if is_hardware_breakpoint == True:
			result.isContinue = BREAKPOINT
			debug_print("HARDWARE BREAKPOINT!!! %d" % hardware_breakpoint_index)
			self.remove_one_hardware_breakpoint(hardware_breakpoint_index)
			context.EFlags ^= 0x100	# set the trap flag
			return

		if ex.ExceptionAddress in self.software_breakpoints:
			windll.kernel32.VirtualProtectEx(self.handle, ex.ExceptionAddress, 1, 0x40, byref(oldprotect)) # 0x40 : PAGE_EXECUTE_READWRITE
			size = SIZE_T()
			windll.kernel32.WriteProcessMemory(self.handle, ex.ExceptionAddress, "\xCC", 1, byref(size))
			windll.kernel32.VirtualProtectEx(self.handle, ex.ExceptionAddress, 1, oldprotect, byref(oldprotect)) 

		# should be ...
		'''if ex.ExceptionAddress in self.hardware_breakpoints:'''


		windll.kernel32.SetThreadContext(hThread, byref(context))
		windll.kernel32.CloseHandle(hThread)

		return

	def _process_killed(self, process, debug_event, result):
		self.is_killed = True
		debug_print('Process killed (pid=%d, hProcess=%d)' % (process.pid, process.handle.value))
	def kill(self):
		print "attempt to kill process"
		windll.kernel32.TerminateProcess(self.handle, 0)
		windll.kernel32.CloseHandle(self.handle)
		return
	
	def get_debug_event(self):
		if self.is_killed == True:
			return -1
		debug_event = DEBUG_EVENT()
		continue_status = DBG_CONTINUE
		isContinue = 0
		result = EventResult()
		if windll.kernel32.WaitForDebugEvent(byref(debug_event), INFINITE):
			self.debugger_active = False
			# print 'Debug event received:',
			# print dbgevent_names[debug_event.dwDebugEventCode]
			if debug_event.dwDebugEventCode in self.callbacks:
				for func in self.callbacks[debug_event.dwDebugEventCode]:
					func(self, debug_event, result) # if one is 0, don't continue
			else:
				debug_print("not handled debug event")

			if result.isContinue == DBG_EXCEPTION_NOT_HANDLED:
				continue_status = DBG_EXCEPTION_NOT_HANDLED
			elif result.isContinue == BREAKPOINT:
				debug_print("Breakpoint")
				windll.kernel32.ContinueDebugEvent(
					debug_event.dwProcessId,
					debug_event.dwThreadId,
					continue_status)
			elif result.isContinue == CONTINUE:
				# debug_print("Continue process")
				windll.kernel32.ContinueDebugEvent(
					debug_event.dwProcessId,
					debug_event.dwThreadId,
					continue_status)
			else:
				debug_print("API Error : result.isContinue must be CONTINUE, NOTHANDLED, or BREAKPOINT")
			return debug_event
		else:
			debug_print(`windll.kernel32.GetLastError(), self.handle`)
			# self._process_killed()
			return -1
	def detach(self):
		if windll.kernel32.DebugActiveProcessStop(self.pid):
			return True
		else:
			return False
	def stop(self):
		if windll.kernel32.DebugBreakProcess(self.handle) != 0:
			return True
		else:
			return False

	def bind(self, event, func):
		self.callbacks[event].append(func)

	def unbind(self, event, func):
		self.callbacks[event].remove(func)
	def get_thread_by_tid(self, tid):
		return [t for t in self.threads if t.id == tid]
	def get_main_thread(self):
		return [t for t in self.threads if t.id == self.tid]
	def software_breakpoint(self, addr):
		oldprotect = DWORD()
		size = c_int(0)
		buffer = create_string_buffer(1)
		if windll.kernel32.ReadProcessMemory(
			self.handle,
			addr,
			byref(buffer),
			1,
			byref(size)) == 0:
			print "Warning: cannot read memory at %x" % addr
		if addr in self.software_breakpoints:
			print "Warning: software breakpoint already exists at that location"
		self.software_breakpoints[addr] = buffer

		if windll.kernel32.VirtualProtectEx(self.handle, addr, 1, 0x40, byref(oldprotect)) != 0:  # 0x40 : PAGE_EXECUTE_READWRITE
			ret = windll.kernel32.WriteProcessMemory(
				self.handle,
				addr,
				"\xCC",
				1,
				byref(size))
		else:
			return False
		if windll.kernel32.VirtualProtectEx(self.handle, addr, 1, oldprotect, byref(oldprotect)) == 0:
			return False
		print self.handle, addr, 1, 0x40, oldprotect

		if ret == 0:
			debug_print("_pre_exception BP INIT ERROR!!")
		return ret

	def get_regs():
		context = CONTEXT()
		context.ContextFlags = 0x00010007 | 0x00010010
		if windll.kernel32.GetThreadContext(self.threads[0].handle, byref(context)) == 0:
			return []
		else:
			return context

	def enumerate_threads(self):
		thread_entry = THREADENTRY32()
		snapshot = windll.kernel32.CreateToolhelp32Snapshot(0x00000004, self.pid)
		if snapshot == None:
			return 

		thread_entry.dwSize = sizeof(thread_entry)
		ret = windll.kernel32.Thread32First(snapshot, byref(thread_entry))
		while ret:
			if thread_entry.th32OwnerProcessID == self.pid:
				yield thread_entry.th32ThreadID
			ret = windll.kernel32.Thread32Next(snapshot, byref(thread_entry))

		windll.kernel32.CloseHandle(snapshot)


	def apply_hardware_breakpoint(self, breakpoint):
		assert len(breakpoint) == 4, "what?"
		debug_print("[DEBUG] FUNC CALL : apply_hardware_breakpoint()")

		context = CONTEXT()
		context.ContextFlags = 0x00010007 | 0x00010010

		self.hardware_breakpoints = breakpoint

		for tid in self.enumerate_threads():
			hThread = windll.kernel32.OpenThread(THREAD_GET_CONTEXT | THREAD_SET_CONTEXT, None, tid)
			if hThread == None:
				debug_print("[ERROR] apply_hardware_breakpoint ::::::::::::::: FALSE")
			if windll.kernel32.GetThreadContext(hThread, byref(context)) == None:
				debug_print("[ERROR] apply_hardware_breakpoint ::::::::::::::: FALSE")

			for sel in range(0, 4):
				debug_print("[DEBUG] apply_hardware_breakpoint ::::::::::::::: sel %x" % breakpoint[sel][0])
				if breakpoint[sel][0] == -1: continue
				debug_print("[DEBUG] breakpoint : %x %x %x %x " % (sel, breakpoint[sel][0], breakpoint[sel][1], breakpoint[sel][2]))
				if sel == 0: context.Dr0 = DWORD(breakpoint[sel][0])
				elif sel == 1: context.Dr1 = DWORD(breakpoint[sel][0])
				elif sel == 2: context.Dr2 = DWORD(breakpoint[sel][0])
				elif sel == 3: context.Dr3 = DWORD(breakpoint[sel][0])

				context.Dr7 |= 1 << (sel * 2)
				context.Dr7 |= breakpoint[sel][1] << (16 + (sel * 4))
				context.Dr7 |= breakpoint[sel][2] << (18 + (sel * 4))

			if windll.kernel32.SetThreadContext(hThread, byref(context)) == None:
				debug_print("[ERROR] apply_hardware_breakpoint")

			windll.kernel32.CloseHandle(hThread)

	def remove_one_hardware_breakpoint(self, sel):
		context = CONTEXT()
		context.ContextFlags = 0x00010007 | 0x00010010

		for tid in self.enumerate_threads():
			hThread = windll.kernel32.OpenThread(THREAD_GET_CONTEXT | THREAD_SET_CONTEXT, None, tid)
			if hThread == None:
				debug_print("[ERROR] remove_one_hardware_breakpoint ::::::::::::::: FALSE")
			if windll.kernel32.GetThreadContext(hThread, byref(context)) == None:
				debug_print("[ERROR] remove_one_hardware_breakpoint ::::::::::::::: FALSE")

			if self.hardware_breakpoints[sel][0] == -1: return

			if sel == 0: context.Dr0 = 0
			elif sel == 1: context.Dr1 = 0
			elif sel == 2: context.Dr2 = 0
			elif sel == 3: context.Dr3 = 0

			context.Dr7 &= 0xFFFFFFFF ^ (1 << (sel * 2))
			context.Dr7 &= 0xFFFFFFFF ^ (0b11 << (16 + (sel * 4)))
			context.Dr7 &= 0xFFFFFFFF ^ (0b11 << (18 + (sel * 4)))

			if windll.kernel32.SetThreadContext(hThread, byref(context)) == None:
				debug_print("[ERROR] remove_one_hardware_breakpoint")
			windll.kernel32.CloseHandle(hThread)





class dThread():
	def __init__(self, handle, *args, **kwargs):
		self.handle = handle
		self.hThread = handle
		self.tid = None
		self.base_addr = 0xFFFFFFFF
		for key, value in kwargs.items():
			if key == 'handle':
				self.handle = value
			elif key == 'tid':
				self.tid = value
			elif key == 'base_addr':
				self.base_addr = value
		if self.handle is not None and self.tid is None:
			self.tid = windll.kernel32.GetThreadId(self.handle)
		return
	def trap(self):
		# not yet
		context = CONTEXT()
		context.ContextFlags = 0x00010007 | 0x00010010
		if windll.kernel32.GetThreadContext(self.handle, byref(context)) == 0:
			return False
		context.Eflags |= 0x100 # set trap flag
		if windll.kernel32.SetThreadContext(self.handle, byref(context)) == 0:
			return False
	def stop(self):
		return windll.kernel32.StopThread(self.handle)
	def resume(self):
		return windll.kernel32.ResumeThread(self.handle)
	def if_exist(self):
		handle = windll.kernel32.OpenThread(8, True, self.tid)
		if handle == 0:
			return False
		else:
			windll.kernel32.CloseHandle(handle)
	def kill(self):
		return windll.kernel32.ExitThread(self.handle)

class EventResult:
	isContinue = CONTINUE