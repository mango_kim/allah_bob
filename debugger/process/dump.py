from ctypes import *
from structure import *
from logger import *

class dProcess_DUMP:
	def dump_area(self, start, end):
		size = end - start
		if size < 0:
			return False
		buffer = create_string_buffer(size)
		result = windll.kernel32.ReadProcessMemory(self.handle,
			start,
			byref(buffer),
			size,
			byref(size))
		if result == 0:
			return False
		else:
			return buffer

	def dump_area2(self, lpAddress):
		mbi = virtualqueryex(self.handle, lpAddress)
		buffer = c_buffer(mbi.RegionSize)
		ret = windll.kernel32.ReadProcessMemory(self.handle, mbi.AllocationBase, buffer, mbi.RegionSize, c_ulong(0))
		assert ret, "dump_area fail.  %s\n" % WinError (GetLastError ()) [1]
		return buffer

	def virtualqueryex(self, lpAddress):
		memory_basic_information = MEMORY_BASIC_INFORMATION()
		ret = windll.kernel32.VirtualQueryEx(self.handle, lpAddress, byref(memory_basic_information), sizeof(memory_basic_information))
		#assert ret, "virtualqueryex() error. %s\n" % WinError (GetLastError ()) [1]
		return memory_basic_information if ret != 0 else None

	def mem_region(self):
		addr = 0
		size = c_ulong(1)
		region = []
		while size > 0:
			memory_basic_information = self.virtualqueryex(addr)
			if memory_basic_information is None:
				break
			allocationbase = int(0 if memory_basic_information.AllocationBase is None else memory_basic_information.AllocationBase)
			size = int(0 if memory_basic_information.RegionSize is None else memory_basic_information.RegionSize)
			if allocationbase == 0:
				addr += size
				continue
			debug_print('AllocationBase : 0x%x RegionSize : 0x%x' % (allocationbase, size))
			region.append((alocationbase, size))
			addr += size

