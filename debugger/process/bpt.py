from constant import *
from ctypes import *
from logger import *
from structure import *

class dProcess_BPT:
	guarded_pages = []
	def memory_breakpoint(self, addr, size):
		oldProtect = c_ulong(0)
		sysinfo = SYSTEM_INFO()
		windll.kernel32.GetSystemInfo(byref(sysinfo))
		self.page_size = sysinfo.dwPageSize
		mbi = MEMORY_BASIC_INFORMATION()
		debug_print(`self.handle`)
		handle = windll.kernel32.OpenProcess(0x408, True, self.pid)
		print handle, self.pid
		if handle == 0:
			return False
		if windll.kernel32.VirtualQueryEx(handle,addr,byref(mbi),sizeof(mbi)) < sizeof(mbi):
			return False
		curr_page = mbi.BaseAddress ## from init???
		#if windll.kernel32.VirtualProtect(addr & 0xFFFFFF000, size, PAGE_GUARD, byref(oldProtect)) == 0:
		while curr_page <= addr + size:
			self.guarded_pages.append(curr_page)
			oldProtect = c_ulong(0)
			debug_print(`mbi.Protect`)
			if not windll.kernel32.VirtualProtectEx(handle,addr,size,mbi.Protect|PAGE_GUARD,byref(oldProtect)):
				exit(0) ##hi
			curr_page += self.page_size	
			self.memory_breakpoints[addr] = (addr,size,mbi)
			return 1
	def apply_hardware_breakpoint(self, hbpts):
		self.hardware_breakpoints = hbpts
		for thread in self.threads:
			self.hardware_breakpoint_by_thread(hbpts, thread)
		return
	def hardware_breakpoint_by_thread(self, hbpts, thread):
		hThread = windll.kernel32.OpenThread(0x0010 | 0x0008, True, thread.tid)
		if hThread == 0:
			debug_print("OpenThead Failed")
			return False
		context = CONTEXT()
		windll.kernel32.GetThreadContext(hThread, byref(context))
		bpt_index = 0
		context.Dr7 = 0
		for bpt_addr, bpt_type, bpt_size in self.hardware_breakpoints:
			if bpt_addr == -1:
				continue
			for i in [0, 1, 2, 3, 6, 7]:
				print "Dr%d : %d" % (i, eval("context.Dr%d" % i))
			exec ("context.Dr%d = %d" % (bpt_index, bpt_addr))
			context.Dr7 |= (bpt_type << (bpt_index * 3)) << 16
			context.Dr7 |= 3 << (bpt_index * 2)

			if windll.kernel32.SetThreadContext(hThread, byref(context)) == 0:
				debug_print("SetThreadContext Failed")
				return False
			debug_print("Hardware breakpoint registered : 0x%08x, size=%d" % (bpt_addr, bpt_size))
			for i in [0, 1, 2, 3, 6, 7]:
				print "Dr%d : %d" % (i, eval("context.Dr%d" % i))
			bpt_index += 1
	def sbpt_list(self):
		return self.software_breakpoints
	def mbpt_list(self):
		return self.memory_breakpoints
	def hbpt_list(self):
		return self.hardware_breakpoints
	def continue_process(self):
		return
	def del_breakpoint(self):
		return