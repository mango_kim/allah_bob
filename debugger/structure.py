import ctypes

WORD   = ctypes.c_ushort
DWORD  = ctypes.c_uint
LPSTR  = ctypes.c_char_p
LPWSTR = ctypes.c_wchar_p
LPVOID = ctypes.c_void_p
ULONGLONG = ctypes.c_ulonglong
LPBYTE = LPSTR
HANDLE = DWORD
BYTE = ctypes.c_char
PVOID     = ctypes.c_void_p
SIZE_T    = ctypes.c_ulong

class MEMORY_BASIC_INFORMATION32(ctypes.Structure):
	_fields_ = [
		('BaseAddress', PVOID),
		('AllocationBase', PVOID),
		('AllocationProtect', DWORD),
		('RegionSize', SIZE_T),
		('State', DWORD),
		('Protect', DWORD),
		('Type', DWORD)
	]

# currently, unused (64-bit)
class MEMORY_BASIC_INFORMATION64(ctypes.Structure):
	_fields_ = [
		('BaseAddress', ULONGLONG),
		('AllocationBase', ULONGLONG),
		('AllocationProtect', DWORD),
		('__alignment1', DWORD),
		('RegionSize', ULONGLONG),
		('State', DWORD),
		('Protect', DWORD),
		('Type', DWORD),
		('__alignment2', DWORD)
	]

MEMORY_BASIC_INFORMATION = MEMORY_BASIC_INFORMATION32

 # typedef struct _PROCESS_INFORMATION {
 #	 HANDLE hProcess;
 #	 HANDLE hThread;
 #	 DWORD dwProcessId;
 #	 DWORD dwThreadId;
 # } PROCESS_INFORMATION, *PPROCESS_INFORMATION, *LPPROCESS_INFORMATION;
class PROCESS_INFORMATION(ctypes.Structure):
	_pack_   = 1
	_fields_ = [
	  ('hProcess',	HANDLE),
	  ('hThread',	 HANDLE),
	  ('dwProcessId', DWORD),
	  ('dwThreadId',  DWORD),
	]

 # typedef struct _STARTUPINFO {
 #	 DWORD   cb;
 #	 LPSTR   lpReserved;
 #	 LPSTR   lpDesktop;
 #	 LPSTR   lpTitle;
 #	 DWORD   dwX;
 #	 DWORD   dwY;
 #	 DWORD   dwXSize;
 #	 DWORD   dwYSize;
 #	 DWORD   dwXCountChars;
 #	 DWORD   dwYCountChars;
 #	 DWORD   dwFillAttribute;
 #	 DWORD   dwFlags;
 #	 WORD	wShowWindow;
 #	 WORD	cbReserved2;
 #	 LPBYTE  lpReserved2;
 #	 HANDLE  hStdInput;
 #	 HANDLE  hStdOutput;
 #	 HANDLE  hStdError;
 # } STARTUPINFO, *LPSTARTUPINFO;
class STARTUPINFO(ctypes.Structure):
	_pack_   = 1
	_fields_ = [
	  ('cb',		  DWORD),
	  ('lpReserved',	DWORD),	 # LPSTR
	  ('lpDesktop',	 LPSTR),
	  ('lpTitle',	   LPSTR),
	  ('dwX',		 DWORD),
	  ('dwY',		 DWORD),
	  ('dwXSize',	   DWORD),
	  ('dwYSize',	   DWORD),
	  ('dwXCountChars',   DWORD),
	  ('dwYCountChars',   DWORD),
	  ('dwFillAttribute', DWORD),
	  ('dwFlags',	   DWORD),
	  ('wShowWindow',	 WORD),
	  ('cbReserved2',	 WORD),
	  ('lpReserved2',	 DWORD),	 # LPBYTE
	  ('hStdInput',	 DWORD),
	  ('hStdOutput',	DWORD),
	  ('hStdError',	 DWORD),
	]
class  COORD(ctypes.Structure):
	  _fields_=[("X",ctypes.c_short),
	("Y",ctypes.c_short)
	]

class SMALL_RECT(ctypes.Structure):
	_fields_=[("Left",ctypes.c_short),
		("Top",ctypes.c_short),
		("Right",ctypes.c_short),
		("Bottom",ctypes.c_short)
	]

class CONSOLE_SCREEN_BUFFER_INFO(ctypes.Structure):
	_fields_=[("Size",COORD),
		("CursorPosition",COORD),
		("Attributes",ctypes.c_short),
		("Window",SMALL_RECT),
		("MaximumWindowSize",COORD)
	]
class EXCEPTION_RECORD(ctypes.Structure):
    pass

EXCEPTION_RECORD._fields_ =  [('ExceptionCode', DWORD),
                              ('ExceptionFlags', DWORD),
                              ('ExceptionRecord', ctypes.POINTER(EXCEPTION_RECORD)),
                              ('ExceptionAddress', LPVOID),
                              ('NumberParameters', DWORD),
                              ('ExceptionInformation', ctypes.POINTER(DWORD) * 15),
                             ]
#http://msdn.microsoft.com/en-us/library/windows/desktop/ms679326(v=vs.85).aspx
class EXCEPTION_DEBUG_INFO(ctypes.Structure):
    _fields_ = [('ExceptionRecord', EXCEPTION_RECORD),
                ('dwFirstChance', DWORD),
               ]

#http://msdn.microsoft.com/en-us/library/windows/desktop/ms679287(v=vs.85).aspx
class LPTHREAD_START_ROUTINE(ctypes.Structure):
	_fields_=[('lpThreadParameter', LPVOID)]
class CREATE_THREAD_DEBUG_INFO(ctypes.Structure):
    _fields_ = [ ('hThread', HANDLE),
                 ('lpThreadLocalBase', LPVOID),
                 ('lpStartAddress', LPTHREAD_START_ROUTINE),
               ]

class CREATE_PROCESS_DEBUG_INFO(ctypes.Structure):
	_fields_ = [
		('hFile', HANDLE),
		('hProcess', HANDLE),
		('hThread', HANDLE),
		('lpBaseOfImage', LPVOID),
		('dwDebugInfoFileOffset', DWORD),
		('nDebugInfoSize', DWORD),
		('lpThreadLocalBase', LPVOID),
		('lpStartAddress', LPTHREAD_START_ROUTINE),
		('lpImageName', LPVOID),
		('fUnicode', WORD)
	]

class EXIT_PROCESS_DEBUG_INFO(ctypes.Structure):
	_fields_ = [
		('dwExitCode', DWORD)
	]

class EXIT_THREAD_DEBUG_INFO(ctypes.Structure):
	_fields_ = [
		('dwExitCode', DWORD)
	]

class LOAD_DLL_DEBUG_INFO(ctypes.Structure):
	_fields_ = [
		('hFile', HANDLE),
		('lpBaseOfDll', LPVOID),
		('dwDebugInfoFileOffset', DWORD),
		('nDebugInfoSize', DWORD),
		('lpImageName', LPVOID),
		('fUnicode', WORD)
	]

class UNLOAD_DLL_DEBUG_INFO(ctypes.Structure):
	_fields_ = [
		('lpBaseOfDll', LPVOID)
	]

class OUTPUT_DEBUG_STRING_INFO(ctypes.Structure):
	_fields_ = [
		('lpDebugStringData', LPSTR),
		('fUnicode', WORD),
		('nDebugStringLength', WORD)
	]

class RIP_INFO(ctypes.Structure):
	_fields_ = [
		('dwError', DWORD),
		('dwType', DWORD)
	]

class _DEBUG_EVENT_UNION_(ctypes.Union): 
	_fields_ = [ 
		('Exception',		 EXCEPTION_DEBUG_INFO), 
		('CreateThread',	  CREATE_THREAD_DEBUG_INFO), 
		('CreateProcessInfo',   CREATE_PROCESS_DEBUG_INFO), 
		('ExitThread',		EXIT_THREAD_DEBUG_INFO), 
		('ExitProcess',	   EXIT_PROCESS_DEBUG_INFO), 
		('LoadDll',		 LOAD_DLL_DEBUG_INFO), 
		('UnloadDll',		 UNLOAD_DLL_DEBUG_INFO), 
		('DebugString',	   OUTPUT_DEBUG_STRING_INFO), 
		('RipInfo',		 RIP_INFO), 
	] 
class DEBUG_EVENT(ctypes.Structure):
	_fields_=[('dwDebugEventCode', DWORD),
		('dwProcessId', DWORD),
		('dwThreadId', DWORD),
		('u', _DEBUG_EVENT_UNION_)
	]


class MEMORY_BASIC_INFORMATION(ctypes.Structure):
    _fields_ = [("BaseAddress", PVOID),
                ("AllocationBase", PVOID),
                ("AllocationProtect", DWORD),
                ("RegionSize", SIZE_T),
                ("State", DWORD),
                ("Protect", DWORD),
                ("Type", DWORD),]

"""

copied from http://www.ttgnet.com/daynotes/2009/2009-52.html
in 

"""
SIZE_OF_80387_REGISTERS = 80
class FLOATING_SAVE_AREA(ctypes.Structure):
    _fields_ = [('ControlWord', DWORD),
                ('StatusWord', DWORD),
                ('TagWord', DWORD),
                ('ErrorOffset', DWORD),
                ('ErrorSelector', DWORD),
                ('DataOffset', DWORD),
                ('DataSelector', DWORD),
                ('RegisterArea', BYTE * SIZE_OF_80387_REGISTERS),
                ('Cr0NpxState', DWORD)]

MAXIMUM_SUPPORTED_EXTENSION = 512
class CONTEXT(ctypes.Structure):
    _fields_ = [('ContextFlags', DWORD),
                ('Dr0', DWORD),
                ('Dr1', DWORD),
                ('Dr2', DWORD),
                ('Dr3', DWORD),
                ('Dr6', DWORD),
                ('Dr7', DWORD),
                ('FloatSave', FLOATING_SAVE_AREA),
                ('SegGs', DWORD),
                ('SegFs', DWORD),
                ('SegEs', DWORD),
                ('SegDs', DWORD),
                ('Edi', DWORD),
                ('Esi', DWORD),
                ('Ebx', DWORD),
                ('Edx', DWORD),
                ('Ecx', DWORD),
                ('Eax', DWORD),
                ('Ebp', DWORD),
                ('Eip', DWORD),
                ('SegCs', DWORD),
                ('EFlags', DWORD),
                ('Esp', DWORD),
                ('SegSs', DWORD),
                ('ExtendedRegisters', BYTE * MAXIMUM_SUPPORTED_EXTENSION)]
LPCONTEXT = ctypes.POINTER(CONTEXT)

class THREADENTRY32(ctypes.Structure):
    _fields_ = [
        ("dwSize",             DWORD),
        ("cntUsage",           DWORD),
        ("th32ThreadID",       DWORD),
        ("th32OwnerProcessID", DWORD),
        ("tpBasePri",          DWORD),
        ("tpDeltaPri",         DWORD),
        ("dwFlags",            DWORD),
    ]