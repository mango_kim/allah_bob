from console import *
from logger import *
import msvcrt
from sys import *

cmd_data = ((open("./cmd_list.txt","r")).read().strip()).split("\n")
cmd_help = dict(map(tuple, map(lambda x: x.split(' ',1), cmd_data)))
cmd_list = map(lambda x: x.split(' ',1)[0], cmd_data)

def ask_die():
	if choice("quit??"):
		print "bye bye..."
		exit(0)
flag = 0
damnlen = 0
yura = 0
fline = ""
woop = ""
def autocmd(his):
	for a in cmd_list:
		if a.startswith(his) == True:
			damnlen = len(his)
			yura = len(his)
			flag = 1
			return a[yura:]
	return woop

def getch(line_until_now):	
	usrinput = msvcrt.getch()
	if type(usrinput) == int:
		userinput = chr(userinput)
	if usrinput == "\x03":		#if ctrl + c
		return 2
	elif usrinput == "\x04":	#if ctrl + d
		ask_die()
	elif usrinput == "\x09":
		return autocmd(line_until_now)
	else:
		return usrinput	#if just char

def filter(real_cmd):
	if real_cmd == "blah":
		print ""

def argv_parse(argv):
	if type(argv) == list:
		return argv
	else:
		argv = argv.split(" ")
		return argv

def shell():
	global fline
	fline = ""
	while True:
		global woop
		woop = ""
		global flag
		global damnlen
		woop = getch(fline)
		if woop == "\x02":
			return (-3,fline)		#if ctrl+c run.py must enter
		elif woop == "\x03":
			print "ctrl+c"
			flush()
			break
		elif woop == "\x0a" or woop == "\x0d":
			flush()
			debug_print("")
			break
		elif woop == "\x08" or flag == 1:
			fline = fline[:-1]
			if damnlen != 0:
				for i in range(damnlen):
					stdout.write(d_record())
			else:
				stdout.write(d_record())
			flag = 0
		elif woop == "\xe0":
			sub_woop = getch(fline)
			# shell history not implemented
		else:
			fline += str(woop)
			record(str(woop))
			stdout.write(woop)
	line = fline.strip().split(" ")
	#line = raw_input('[allah] ').strip().split(' ')
	if len(line) == 1 and line[0] == '':
		return (0, line)
	cmd = line[0]
	argv = line[1:]		#need to parse argv to.
	if cmd == 'help':
		if len(argv) >= 2:
			if argv[1] in cmd_help:
				debug_print(' '.join(cmd_help[argv]))
			else:
				debug_print('Invalid command : %s' % argv[1])
		else:
			print '\n'.join(cmd_data)
	if len(argv) > 1:
		argv = argv_parse(argv)
	match_cnt = 0
	real_cmd = None
	cmds = []
	# Why commented these line?
	for target_cmd in cmd_list:
		if target_cmd.startswith(cmd) == True:
			cmds.append(target_cmd)
			real_cmd = target_cmd
			match_cnt += 1
	if match_cnt > 1:
		return (-2,[cmd, cmds])
	elif match_cnt == 0:
		return (-1, line)
	filter(real_cmd)	########################
	flush()
	return (real_cmd, argv)


def choice(string):
	while True:
		choice = raw_input(string+" (y/n) ")
		if choice == 'y':
			return True
		elif choice == 'n':
			return False
		else:
			pass
rows = 0
cols = 0
def view_more():
	while True:
		choice = raw_input("**** wanna see more??? <return> to continue, <q> to quit **** ")
		if choice == '':	#return	
			cols, rows = terminal_size()
			return rows 	#return tty space
		elif choice == ' ':	#space
			return 1	#return 1
		elif choice == 'q':	#bye
			return False
		else:
			pass

def banner():
	debug_print("""
	Koallah Debugger version 0.0.1
	type "help" to see command usages
	""")
