# Codename Ko-allah #

코알라 디버거는 api가 잘 되어있는 짱짱 콘솔 디버거입니다.

### 뭐죠? ###

* gdb같은 콘솔 디버깅
* 3가지 브릭포인트 지원 (TODO)
* 메모리 덤프 지원 (TODO)
* [마크다운 메모 어케하지?](https://bitbucket.org/tutorials/markdowndemo)

### 까는법 ###

* run.py를 실행한다.
* 마음에 안드는가? 직접 고친다.
* 이슈를 만든 후 자기한테 assign해서 고친다.
* 걍 내비둔다.
* 이슈만 만든다.
* 나는 짱짱 개발실력으로 커밋한다음에 배포해준다.

### 코딩 가이드라인 ###

* ? 클래스를 썼네? 와! 멋지다!
* 알아보기 쉽게 작성하자.
* 주석 없어도 알아보기 쉽게 작성하자. 꼭!

### 마음에 안드는 기능이 있어! ###

* 이슈 등록
* 직접 개발하고 싶으면:
  자신의 이름으로 브랜치 생성
  자신의 브랜치로 체크아웃
  개발 열심히 함 (커밋 단위는 조그맣게 해야 나중에 되돌리기 편해요. 초기 커밋 참조)
  커밋 열심히 함
  어느정도 자기부분 완성 되면 master 브랜치로 체크아웃
  그다음 merge 자기브랜치
  충돌 있을 경우 알아서 잘 diff 툴 보고 고쳐서 넣으면 됨 (diff 툴 사용법 먼저 숙지 바람)